const input1 = document.querySelectorAll("input")[0];
const input2 = document.querySelectorAll("input")[1];
const button = document.querySelector(".btn");

const icon1 = document.querySelectorAll(".fas")[0];
const icon2 = document.querySelectorAll(".fas")[1];

const message = document.querySelector(".message");

// change icon 1 -> icon 2

// icon 1
icon1.addEventListener("click", function () {
  icon1.classList.contains("fa-eye-slash")
    ? icon1.classList.remove("fa-eye-slash")
    : icon1.classList.add("fa-eye-slash");

  input1.type === "text"
    ? (input1.type = "password")
    : (input1.type = "text");
});

// icon 2
icon2.addEventListener("click", function () {
  icon2.classList.contains("fa-eye-slash")
    ? icon2.classList.remove("fa-eye-slash")
    : icon2.classList.add("fa-eye-slash");

  input2.type === "text"
    ? (input2.type = "password")
    : (input2.type = "text");
});

// compare values of inputs
button.addEventListener("click", function () {
  let valueinput1 = input1.value;
  let valueinput2 = input2.value;
  if (valueinput1 === valueinput2) {
    message.textContent = alert("You are welcome!");
  } else {
      message.textContent = "Потрібно ввести однакові значення";
        message.style.color = "red";
  }
});


